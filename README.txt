Hoover
======

The Hoover module logs events by sending messages to Loggly. Loggly is cloud
based service for complete application intelligence for app developers. Loggly
uses log data to collect, analyze, troubleshoot and monitor your applications.


Requirements
------------

This module requires that you have an account with Loggly (http://loggly.com/)


Installation / Configuration
---------------------------

  * Copy the module's directory to your modules directory and activate the
    module.

  * Create a new HTTP input (optional enable JSON) in Loggly.

  * Add the input key from Loggly in admin/config/development/hoover.

  * To send drupal events to Loggly, check "Log Drupal events".
    - If you enabled JSON for the input, check "JSON Logging".
    - To send messages in the background set the cron interval.

  * To use the JavaScript logger, check "Enable JavaScript logger".
    + You can use this option without the "Log Drupal events" setting.

  * Customise the log message if needed.


Maintainers
-----------

Module created by Richard Banks (SubOracle)
  * http://drupal.org/user/55341
  * http://richardbanks.net/
